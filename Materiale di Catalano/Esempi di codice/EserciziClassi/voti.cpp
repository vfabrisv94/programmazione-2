/* 
Corso di Programmazione II (A-L), Prof. Dario Catalano 

Esercizi sulle classi

Esercizio 4: Definizione di una classe voti. 
*/
#include<iostream>
using namespace std;

#define MAX 100

class Voti	{
		double voti[MAX]; // campo privato
		int n; 
	public: 
		Voti(); // Costruttore
		void inserisci(double voto); 
		double media_voti(); 
		//double voto_massimo();
		//double voto_minimo();
		};

void Voti::inserisci(double voto) 
{
	voti[n++]=voto; 
}
 
Voti::Voti() {
	n=0;
	for(int i=0; i<MAX;i++)
		voti[i]=0;
	}	

double Voti::media_voti() {
	double somma=0; 
	for(int i=0;i<n;i++) 
		somma+=voti[i];
	return (somma/n);
	}

int main()
{
	Voti voti; 
	bool flag=1;
	//voti.Voti(); 
	// Il costruttore non deve essere 
	// invocato esplicitamente
	cout << "Inserisci i voti (e un numero negativo per terminare)\n"; 
	do {
		// bool flag=1;
		// Dichiarare flag qua darebbe errore perché 
		// la variabile sarebb invisibile al while
		double voto; 
		cout << "-- ";
		cin >> voto;
		if (voto<0) flag=0;
		else voti.inserisci(voto); 
	}	while(flag);
	double media=voti.media_voti();
	cout << "La media è: " << media << endl; 
	
} 