/* 
Corso di Programmazione II (A-L), Prof. Dario Catalano 

Esercizi sui puntatori

Esercizio 3: Scrittura dell'alfabeto usando i puntatori
*/
#include<iostream>
using namespace std;

char c;

int main()
{ 
	char* primo; 
	primo=&c;
	
	for (c='A'; c <='Z'; c++) 
		cout << *primo << ", "; 
	return 0; 
}