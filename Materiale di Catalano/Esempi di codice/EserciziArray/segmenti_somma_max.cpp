/* 
Corso di Programmazione II (A-L), Prof. Dario Catalano 

Esercizi sugli array

Esercizio: Segmenti di Somma Massima
*/
#include<iostream>
using namespace std;
#define DIM 12

int somma_massima(int a[],int n)
//Proviamo tutti i segmenti fino a quando non si trova quello giusto
{	int max=0;
	
	for(int i=0; i<n;i++) 
		for(int j=i;j<n; j++)
		{	
			int somma=0; 
			for(int k=i;k<=j;k++)
				somma += a[k];
			if (somma > max) max=somma; 
			//cout << "Massimo " << max << endl; 
		}
	return max;
}				
		

int main()
{ 
	int linea[DIM]={13,-5,12,1,-4,16,12,7,19,2,27,-21}; 
	int massimo;
	
	massimo=somma_massima(linea,DIM);
	cout << "Il massimo è " << massimo << endl;
	return 0; 
}

