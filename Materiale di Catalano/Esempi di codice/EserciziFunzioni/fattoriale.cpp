/* 
Corso di Programmazione II (A-L), Prof. Dario Catalano 

Esercizi sulle funzioni

Esercizio 4: Calcolo del fattoriale (versione ricorsiva)
*/
#include<iostream>
using namespace std;


double fattoriale(int n) 
{  
	if (n > 1) return (n*fattoriale(n-1)); 
	else return 1;
}



int main()
{ int n; 

 cout << "Inserisci un intero :" ;
 cin >> n;
 cout << "Il fattoriale di " << n << " è: " << fattoriale(n) << endl;  
 return 0; 
}