#include <iostream>
using namespace std;


template <class A> class Node{
	private:
			A key; 
			Node <A>* next, *prev;
	public:
			Node(A key) {
				this->key = key;
				next = prev = NULL;
			}
			
			Node() {
				next = prev = NULL;
			}
			
			void setNext(Node<A>* next){ this->next = next; }
			void setPrev(Node<A>* prev){ this->prev = prev; }
			void setKey(A key){ this->key = key; }
			
			Node <A>* getNext(){ return this->next; }
			Node <A>* getPrev(){ return this->prev; }
			A getKey() { return this->key; }
			A* getPToKey() { return &key; }
};

template <class P> class Table {
		public:
		virtual void del(P x) = 0;
		virtual void insert(P x) = 0;
		virtual P* max() = 0;
		virtual P* min() = 0;
		virtual void print() = 0;
};

template <class H> class MyTable : public Table <H> {
	private:
			Node <H>* header, * trailer;
			int size;
	public:
			MyTable(){
				size = 0;
				header = new Node<H>();
				trailer = new Node<H>();
				header->setNext(trailer);
				trailer->setPrev(header);
			}
			
			void insert(H x) {
				Node <H>* newNode = new Node<H>(x);
				header->getNext()->setPrev(newNode);
				newNode->setNext(header->getNext());
				newNode->setPrev(header);
				header->setNext(newNode);
				size++;
				return;
			}
			void print(){
				Node <H>* temp = header->getNext();
				while(temp != trailer) {
					cout << temp->getKey() << " ";
					temp = temp->getNext();
				}
				cout << endl;
			}
			
			Node <H>* search(H x) {
				if(!size) return NULL;
				Node <H>* temp = header->getNext();
				while(temp != trailer) {
					if(temp->getKey() == x) return temp;
					temp = temp->getNext();
				}
				return NULL;
			}
			void del(H x){
				if(!size) return;
				Node <H>* temp = search(x);
				if(!temp) return;
				temp->getPrev()->setNext(temp->getNext());
				temp->getNext()->setPrev(temp->getPrev());
				size--;
			}
			
			H* max(){
				if(!size) return NULL;
				Node <H>* temp = header->getNext();
				H* max = temp->getPToKey();
				while(temp != trailer) {
					if(temp->getKey() > *max ) max = temp->getPToKey(); 
					temp = temp->getNext();
				}
				return max;
			}
			
			H* min(){
				if(!size) return NULL;
				Node <H>* temp = header->getNext();
				H* min = temp->getPToKey();
				while(temp != trailer) {
					if(temp->getKey() < *min ) min = temp->getPToKey(); 
					temp = temp->getNext();
				}
				return min;
			}
};

template <class Q> class OrderedTable: public MyTable<Q>{
	private:
			Node <Q>* header, *trailer;
			int size;
	public:
			OrderedTable(){
				header = new Node <Q>();
				trailer = new Node<Q>();
				header->setNext(trailer);
				trailer->setPrev(header);
				size = 0;
			}
			void insert(Q x) {
				Node <Q>* newNode = new Node<Q>(x);
				Node <Q>* temp = header->getNext();
				while(temp != trailer){
					if(temp->getKey() > x) break;
					temp = temp->getNext();
				}
				temp->getPrev()->setNext(newNode);
				newNode->setPrev(temp->getPrev());
				newNode->setNext(temp);
				temp->setPrev(newNode);
				size++;
				return;
			}
			
			void print(){
				Node <Q>* temp = header->getNext();
				while(temp != trailer) {
					cout << temp->getKey() << " ";
					temp = temp->getNext();
				}
				cout << endl;
			}
			
				void del(Q x){
				if(!size) return;
				Node <Q>* temp = search(x);
				if(!temp) return;
				temp->getPrev()->setNext(temp->getNext());
				temp->getNext()->setPrev(temp->getPrev());
				size--;
			}
			
			Q* max(){
				if(!size) return NULL;
				Node <Q>* temp = header->getNext();
				Q* max = temp->getPToKey();
				while(temp != trailer) {
					if(temp->getKey() > *max ) max = temp->getPToKey(); 
					temp = temp->getNext();
				}
				return max;
			}
			
			Q* min(){
				if(!size) return NULL;
				Node <Q>* temp = header->getNext();
				Q* min = temp->getPToKey();
				while(temp != trailer) {
					if(temp->getKey() < *min ) min = temp->getPToKey(); 
					temp = temp->getNext();
				}
				return min;
			}
			
			Node <Q>* search(Q x) {
				if(!size) return NULL;
				Node <Q>* temp = header->getNext();
				while(temp != trailer) {
					if(temp->getKey() == x) return temp;
					temp = temp->getNext();
				}
				return NULL;
			}
};

template <class M> class CircularTable : public MyTable<M> {
	private:
			Node<M>* head, * tail;
			int size;
	public:
			CircularTable(){
				head = tail = NULL;
				size = 0;
			}
			
			void insert(M x){ // INSERIMENTO AFTER TAIL
				if(!head) {
					head = new Node <M>(x);
					tail = head;
					size++;
					return;
				}
				
				Node <M>* temp = new Node<M>(x);
				head->setPrev(temp);
				temp->setNext(head);
				tail->setNext(temp);
				temp->setPrev(tail);
				tail = temp;
				size++;
				return;
			}
		void print(){
			Node <M>* temp = head;
			while( temp!= tail) {
				cout << temp->getKey() << " "; 
				temp = temp->getNext();
			}
			cout<< tail->getKey() << " " << endl; 
		}
		
			Node <M>* search(M x){
				Node <M>* temp = head;
				if(tail->getKey() == x) return tail;
				while(temp != tail){
					if(temp->getKey() == x) return temp;
					temp = temp->getNext();
				}
				return NULL;
			}
			
			void del(M x){
				Node <M>* temp = search(x);
				if(!temp) return;
				temp->getPrev()->setNext(temp->getNext());
				temp->getNext()->setPrev(temp->getPrev());
				if(temp == tail) tail = tail->getPrev();
				if(temp == head) head = head->getNext();
				size--;
				return;
			}
};

int main() {
	int MyArray[] = {3,7,1,8,5,2,6,1};
	/*
	MyTable<int>* Tabella = new MyTable<int>();
	Tabella->del(8);
	Tabella->del(7);
	Tabella->del(1);
	cout << *Tabella->max() << endl;
	cout << *Tabella->min() << endl;
	Tabella->print();
	
	
	OrderedTable<int>* Ordinata = new OrderedTable<int>();
	for(int i = 0; i < 8; i++ ) Ordinata->insert(MyArray[i]);
	
	Ordinata->del(8);
	Ordinata->del(7);
	Ordinata->del(1);
	
	cout << *Ordinata->max() << endl;
	cout << *Ordinata->min() << endl;
	
	Ordinata->print();
	*/
	 CircularTable<int>* Cerchione = new CircularTable<int>();
	 for(int i = 0; i < 8; i++ ) Cerchione->insert(MyArray[i]);
	Cerchione->print();
	

}
