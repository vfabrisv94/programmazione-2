/************************************************************************************************************
* Una Lista con accesso semi-diretto (SDList) è una lista ordinata e doppiamente linkata a cui è associato
* un array di puntatori (chiamati shortcuts) che permettono di accedere più velocemente ad alcuni nodi della
* lista. Anche i puntatori contenuti all’interno dell’array associato alla lista sono ordinati, in ordine non
* decrescente, rispetto al valore contenuto nel nodo puntato dai relativi link.
* Si crei quindi un’istanza di MySDList<int> e si inseriscano al suo interno i seguenti elementi:
* 7,13,2,6,9,10,21,32,4,12.
* Si esegua in seguito la stampa dei valori inseriti nell’albero attraverso la procedura print. L’output del
* programma sarà quindi: 2,4,6,7,9,10,12,13,21,32
*************************************************************************************************************/
#include <iostream>
using namespace std;

template <class H>
class element
{
  element<H> * next, * prev;
  H key;
  public:
    element(H key)
    {
      this->key=key;
      this->next=NULL;
      this->prev=NULL;
    }
    void set_key(H key){ this->key; }
    void set_next(element<H> * next) { this->next=next; }
    void set_prev(element<H> * prev) { this->prev=prev; }
    H get_key(){ return this->key; }
    element<H> * get_next() { return this->next; }
    element<H> * get_prev() { return this->prev; }
};

template <class H>
class sdlist
{
  public:
    virtual sdlist<H> * ins(H key)=0;
    virtual void print()=0;
};

template <class H>
class mysdlist : public sdlist<H>
{
  element<H> * node;
  public:
    H key;
    element<H> * shortcuts[10];
    element<H> * head;
    mysdlist(){ head=NULL; }
    void shortcuts_f();
    mysdlist<H> * ins(H key);
    void print();
    int search(H key);
    mysdlist<H> * del(H key);
};

/************************************************************************************************************
* L’array di shortcuts associato alla lista dovr`a contenere 10 puntatori, {p0, p1, . . . , p9}, dove lo
* shortcut pi punta al primo nodo della lista il cui elemento e' maggiore di 10 × i, se presente, e punta a
* null altrimenti.
*************************************************************************************************************/
template <class H>
void mysdlist<H>::shortcuts_f()
{
  int i=0, j;
  element<H> * last;

  last=head;
  for(i=0; i<10; i++)
  {
    for(node=last; node!=NULL; node=node->get_next())
    {
       if(node->get_key()>10*i && node->get_key()<(10*(i+1)))
       {
         shortcuts[i]=node;
         last=node;
         break;
       }
       else shortcuts[i]=NULL;
    }

  }
}

/************************************************************************************************************
* (a) SDList<H>* ins(H x)
* aggiunge un nuovo elemento (nella corretta posizione) alla struttura dati e restituisce un puntatore ad un
* oggetto di tipo SDList<H>.
*************************************************************************************************************/
template <class H>
mysdlist<H> * mysdlist<H>::ins(H key)
{
  element<H> * temp, * prec;
  if(key<0 || key>100) cout<<"Impossibile inserire elemento "<<key<<endl;
  else
  {
      if(head==NULL) this->head=new element<H>(key);
      else
      {
        for(node=head; node!=NULL && node->get_key()<key; node=node->get_next()) prec=node;

        temp=new element<H>(key);
        temp->set_next(node);

        if(node==head) head=temp;
        else prec->set_next(temp);

        prec=head;
        for(node=head->get_next(); node!=NULL; node=node->get_next())
        {
          node->set_prev(prec);
          prec=node;
        }
      }
  }
  return this;
}
/************************************************************************************************************
* (b) void print()
* è una procedura che stampa in output gli elementi della lista, seguendo l’ordinamento della struttura.
*************************************************************************************************************/
template <class H>
void mysdlist<H>::print()
{
  cout<<head->get_key();
  for(node=head->get_next(); node!=NULL; node=node->get_next()) cout<<", "<<node->get_key();

}

/************************************************************************************************************
* 2. int search(H x)
* restituisce 1 se l’elemento x è presente nella struttura, 0 altrimenti.
*************************************************************************************************************/
template <class H>
int mysdlist<H>::search(H key)
{
  element<H> * succ;
  succ=head;
  for(int i=0; i<10; i++)
  {
    if(shortcuts[i]!=NULL)
    {
       if(shortcuts[i]->get_key()<=key)
       {
           node=shortcuts[i];
           succ=shortcuts[i+1];
       }
    }
  }
  while(node!=NULL && node->get_key()<=succ->get_key())
  {
    if(node->get_key()==key) return 1;
    node=node->get_next();
  }

  return 0;
}


/************************************************************************************************************
* 3. sdlist<H>* del(H x)
* cancella un’occorrenza dell’elemento x dalla lista, se presente, e restituisce un puntatore ad un oggetto
* di tipo MySDList<H>. Si esegua in seguito la cancellazione, dall’istanza della struttura dati creata al
* passo precedente, degli elementi (nell’ordine) 7, 4, 32, 10. Si stampi in seguito il contenuto della lista.
*************************************************************************************************************/
template <class H>
mysdlist<H> * mysdlist<H>::del(H key)
{
  element<H> * succ, * temp, * after;
  int a=0, value=0;
  succ=head;
  for(int i=0; i<10; i++)
  {
    if(shortcuts[i]!=NULL)
    {
       if(shortcuts[i]->get_key()<=key) node=shortcuts[i];
    }
  }

  while(node!=NULL)
  {
    if(node->get_key()==key)
    {
        if(node==head)
        {
            after=head->get_next();
            head=after;
            head->set_next(after->get_next());
            after->set_prev(NULL);
            break;
        }
        else if(node->get_next()!=NULL)
        {
          after=node->get_next();
          temp=node->get_prev();
          node->set_prev(NULL);
          node->set_next(NULL);
          node=after;
          node->set_prev(temp);
          temp->set_next(node);
        }
        else
        {
            node=node->get_prev();
            node->set_next(NULL);
        }

    }
    else node=node->get_next();
  }
  return this;
}

/************************************************************************************************************
* MAIN
*************************************************************************************************************/
main ()
{
  mysdlist<int> * list=new mysdlist<int>();
  int ins[]={7,13,2,6,9,10,21,32,4,12};
  int el[]={7, 4, 32, 10};
  int returned;
  int i=0;


  for(i=0; i<10; i++) list->ins(ins[i]);
  cout<<"INSERIMENTO AVVENUTO CON SUCCESSO\n";
  cout<<"\nVISUALIZZAZIONE\n";
  list->print();
  list->shortcuts_f();
  cout<<endl;
  cout<<"\nELEMENTI NELLO SHORTCUTS (CONTROLLO FACOLTATIVO)\n";
  for(i=0; i<10; i++)
  {
      if(list->shortcuts[i]!=NULL) cout<<list->shortcuts[i]->get_key()<<" ";
      else cout<<"NULL ";
  }
  int x=10;
  cout<<"\n\nRICERCA DI "<<x<<" (CONTROLLO FACOLTATIVO)";
  returned=list->search(x);
  if(returned==1) cout<<endl<<returned<<" - Elemento "<<x<<" trovato in lista\n";
  else cout<<endl<<returned<<" - Elemento "<<x<<" non trovato in lista\n";

  cout<<"\nELIMINAZIONE (CONTROLLO FACOLTATIVO)";
  for(i=0; i<4; i++)
  {
      cout<<"\nEliminazione di "<<el[i]<<": ";
      list->del(el[i]);
      cout<<"AVVENUTA CON SUCCESSO";
  }
  list->shortcuts_f();
  cout<<endl;
  cout<<"\nVISUALIZZAZIONE DOPO ELIMINAZIONE (CONTROLLO FACOLTATIVO)\n";
  list->print();
  cout<<endl<<endl;
  cout<<"\nELEMENTI NELLO SHORTCUTS DOPO ELIMINAZIONE (CONTROLLO FACOLTATIVO)\n";
  for(i=0; i<10; i++)
  {
      if(list->shortcuts[i]!=NULL) cout<<list->shortcuts[i]->get_key()<<" ";
      else cout<<"NULL ";
  }
   cout<<endl;
}
