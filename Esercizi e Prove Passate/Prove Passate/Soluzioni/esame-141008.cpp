#include <iostream>
using namespace std;
int ra=0;
template <class T>
struct nodo{
	T val;
	int mul;
	nodo<T>*padre;
	nodo<T>*sx;
	nodo<T>*dx;
};
template <class T>
struct nodos{
	nodo<T>* node;
	nodos<T>*next;
};
template <class T>
class stack{ //***LEGGI***  classe non necessaria, creata solo per la versione iterativa della funzione rank (vedi sotto).
public:
	nodos<T>*inizio;
	stack(){ this->inizio = NULL; }
	~stack(){}
	void push(nodo<T>*nuovo){
		if (this->inizio == NULL){
			this->inizio = new nodos < T > ;
			this->inizio->node = nuovo;
			this->inizio->next = NULL;
		}
		else{
			if (nuovo != this->inizio->node){
				nodos<T>*nuovoelem = new nodos < T > ;
				nuovoelem->node = nuovo;
				nuovoelem->next = this->inizio;
				this->inizio = nuovoelem;
			}
		}
	}
	nodo<T>* pop(){
		if (this->inizio == NULL) return NULL;
		else{
			nodo<T>*xpop = new nodo < T > ;
			nodos<T>*xpops = new nodos < T > ;
			xpops = this->inizio;
			xpop = this->inizio->node;
			this->inizio = this->inizio->next;
			delete xpops;
			return xpop;
		}
	}
	bool vuota(){
		return !this->inizio;
	}
	nodo<T>*getinizio(){ return this->inizio->node; }
};
template <class H>
class MultiBST {
public:
	virtual MultiBST<H>* ins(H x) = 0;
	virtual int multiplicity(H x) = 0;
	virtual void inorder() = 0;
};

template <class T>
class MyMultiBST : public MultiBST <T>{
public:
	nodo<T>*inizio;
	MyMultiBST(){ this->inizio = NULL; }
	~MyMultiBST(){}
	MyMultiBST<T>* ins(T x) {
		nodo<T>*nuovo = new nodo<T>;
		nuovo->val = x;
		nuovo->padre = NULL;
		nuovo->sx = NULL;
		nuovo->dx = NULL;
		if (this->inizio == NULL){
			this->inizio = nuovo;
			this->inizio->mul = 1;
		}
		else{
			nodo<T>*app = this->inizio;
			nodo<T>*app2=new nodo<T>;
			while (app != NULL && app->val != nuovo->val){
				app2 = app;
				app->val < nuovo->val ? app = app->dx : app = app->sx;
			}
			if (app == NULL){
				app2->val < nuovo->val ? app2->dx = nuovo : app2->sx = nuovo;
				nuovo->padre = app2;
				nuovo->mul = 1;
			}
			else{
				app->mul += 1;
			}
		}
		return this;
	};
	int multiplicity(T x) {
		nodo <T>* app = this->inizio;

		while (app != NULL && app->val != x){

			app->val < x ? app = app->dx : app = app->sx;
		}
		if (app == NULL){ return 0; }
		else{
			return app->mul;
		}
	};
	void inorder() {
		inorder(this->inizio);
	};
	void inorder(nodo<T>* current){
		if (current->sx != NULL){
			inorder(current->sx);
		}
		for (int i = 0; i< current->mul; i++){
			cout << "	" << current->val;
		}
		if (current->dx != NULL){
			inorder(current->dx);
		}
	}

	void trapianta(nodo<T>*x, nodo<T>*y){
		if (x->padre == NULL) {
			this->inizio = y;
		}
		else{
			if (x->padre->dx == x) { x->padre->dx = y; }
			else {
				x->padre->sx = y;
			}
			if (y != NULL) y->padre = x->padre;
		}
	}
	nodo<T>*minore(nodo<T>*app){
		while (app->sx != NULL) app = app->sx;
		return app;
	}
	MyMultiBST* del(T x){
		nodo<T>*app = this->inizio;
		while (app != NULL && app->val != x){

			app->val < x ? app = app->dx : app = app->sx;
		}
		if (app == NULL) {
			cout << "elemento non presente " << endl;
		}
		else{
			if (app->mul >1) { app->mul--; }
			else{
				if (app->sx == NULL) trapianta(app, app->dx);
				else{
					if (app->dx == NULL) trapianta(app, app->sx);
					else{
						nodo<T>*app2 = minore(app->dx);
						if (app2->padre != app) {
							trapianta(app2, app2->dx);
							app2->dx = app->dx;
							app2->dx->padre = app2;
						}
						trapianta(app, app2);
						app2->sx = app->sx;
						app2->sx->padre = app2;
					}
				}
			}


		}
		return this;
	}

	//*******LEGGI*******
	//versione iterativa di rank , la classe stack � stata creata apposta , molto pi� conveniente la versione ricorsiva (vedi sotto).
	int rank( int x){
		bool done = false;
		int j = 0;
		stack<T>* pila = new stack<T>();
		nodo<T>*current = this->inizio;
		while (!done){
			if (current) {
				pila->push(current);
				current = current->sx;
			}
			else if (pila->vuota())
				done = true;
			else {
				current = pila->pop();
			
				if (current->val < x) j += current->mul;
				current = current->dx;
			}
		}
		return j+1;
		}
	//VERSIONE RICORSIVA RANK 
	/*int rank(int x){  
		rank(this->inizio, x);
		return ra+1;
	}

	void rank(nodo<T>* current,int x){
		if (current->sx != NULL){
			rank(current->sx,x);
		}
		if (current->val < x) ra += current->mul;
		if (current->dx != NULL){
			rank(current->dx,x);
		}
	}*/
};




int main(){
	int dainserire[] = { 10, 7, 7, 23, 30, 4, 1, 5, 9, 5, 1, 7, 1, 9 };
	int daeliminare[] = { 7, 4, 23, 7, 7 };
	int perrank[] = { 5, 9, 30 };
	MyMultiBST<int> *mio = new MyMultiBST <int>();

	for (int i = 0; i<14; i++){
		mio->ins(dainserire[i]);
	}
	cout << "Inserimento dei valori e stampa inorder :: " << endl;
	mio->inorder();
	
	cout << endl << endl << endl;
	for (int i = 0; i<5; i++)
		mio->del(daeliminare[i]);
	cout << "Dopo l'eliminazione degli elementi :: " << endl;
	for (int i = 0; i < 5; i++)
		cout << "\t" << daeliminare[i];
	cout << endl << endl << endl;
	cout<<"Ottengo :: "<<endl;
	mio->inorder();
	cout << endl << endl << endl;
	for (int i = 0; i < 3; i++){
		cout << "Rank di :: " << perrank[i] << " ==> " << mio->rank(perrank[i]) << endl;
		ra = 0;
	}
	getchar();
	return 7;
}
