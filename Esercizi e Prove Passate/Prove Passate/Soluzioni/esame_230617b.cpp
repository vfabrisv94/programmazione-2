#include <iostream>

using namespace std;

class Studente{

	private:
		const char* matricola;
		int v;
		int r;
		int e;

	public:
		Studente(const char* m, int _v, int _r, int _e) : matricola(m), v(_v), r(_r), e(_e) {}

		void setMatricola(const char* m){
			matricola=m;
		}

		void setV(int _v){
			v=_v;
		}

		void setR(int _r){
			r=_r;
		}

		void setE(int _e){
			e=_e;
		}

		const char* getMatricola() const{
			return matricola;
		}

		int getV() const{
			return v;
		}

		int getR() const{
			return r;
		}

		int getE() const{
			return e;
		}

		friend bool operator < (const Studente &si, const Studente &sj){
			if(si.getV() > sj.getV()){
				return true;
			}
			if(si.getV() == sj.getV() && si.getR() > sj.getR()){
				return true;
			}
			if(si.getV() == sj.getV() && si.getR() == sj.getR() && si.getE() < sj.getE()){
				return true;
			}
			return false;
		}

		friend bool operator > (const Studente &si, const Studente &sj){
			return sj<si;
		}

		friend bool operator == (const Studente &si, const Studente &sj){
			return !(si<sj) && !(si>sj);
		}

		friend bool operator != (const Studente &si, const Studente &sj){
			return !(si==sj);
		}

		friend bool operator <= (const Studente &si, const Studente &sj){
			return !(si>sj);
		}

		friend bool operator >= (const Studente &si, const Studente &sj){
			return !(si<sj);
		}

		friend ostream& operator << (ostream &out, const Studente &s){
			return out<<s.getMatricola();
		}
};

template <class H> class Node{
	
	private:
		H key;
		Node<H> *parent, *left, *right;

	public:
		Node(H k, Node<H> *p=0, Node<H> *l=0, Node<H> *r=0) : key(k), parent(p), left(l), right(r){}

		void setKey(H k){
			key=k;
		}

		void setParent(Node<H>* p){
			parent=p;
		}

		void setLeft(Node<H>* l){
			left=l;
		}

		void setRight(Node<H>* r){
			right=r;
		}

		H getKey() const{
			return key;
		}

		Node<H>* getParent() const{
			return parent;
		}

		Node<H>* getLeft() const{
			return left;
		}

		Node<H>* getRight() const{
			return right;
		}
};

template <class H> class BST{
	
	private:
		void _inOrder(Node<H>* aux){
			if(aux){
				_inOrder(aux->getLeft());
				cout<<aux->getKey()<<" ";
				_inOrder(aux->getRight());
			}
		}

		Node<H>* _search(H x){
			Node<H>* aux=root;
			while(aux){
				if(x < aux->getKey()){
					aux=aux->getLeft();
				}
				else if(x > aux->getKey()){
					aux=aux->getRight();
				}
				else{
					return aux;
				}
			}
			return 0;
		}

	protected:
		Node<H>* root;

	public:
		BST(){
			root=0;
		}

		BST<H>* ins(H x){
			if(!root){
				root=new Node<H>(x);
				return this;
			}

			Node<H>* aux=root;
			while(true){
				if(x < aux->getKey()){
					if(!aux->getLeft()){
						Node<H>* n=new Node<H>(x, aux);
						aux->setLeft(n);
						return this;
					}
					else{
						aux=aux->getLeft();
					}
				}
				else{
					if(!aux->getRight()){
						Node<H>* n=new Node<H>(x, aux);
						aux->setRight(n);
						return this;
					}
					else{
						aux=aux->getRight();
					}
				}
			}
		}

		void print(){
			if(root){
				_inOrder(root);
			}
			cout<<endl;
		}

		H* search(H x){
			Node<H>* aux=_search(x);
			return aux ? new H(aux->getKey()) : 0;
		}
};

class Valutazione : public BST<Studente>{

	private:
		Node<Studente>* _min(Node<Studente>* aux){
			while(aux && aux->getLeft()){
				aux=aux->getLeft();
			}
			return aux ? aux : 0;
		}

		Node<Studente>* _succ(Node<Studente>* aux){
			if(aux){
				if(aux->getRight()){
					return _min(aux->getRight());
				}
				else{
					Node<Studente>* parent=aux->getParent();
					while(parent && parent->getRight() && parent->getRight()==aux){
						aux=parent;
						parent->getParent();
					}
					return parent;
				}
			}
			return 0;
		}

		Node<Studente>* _search(int pos){
			if(root){
				Node<Studente>* aux=_min(root);
				for(int i=1; i<pos && aux; i++){
					aux=_succ(aux);
				}
				return aux;
			}
			return 0;
		}

	public:
		Valutazione(){
			BST<Studente>();
		}

		Studente* min(){
			Node<Studente>* aux=_min(root);
			return aux ? new Studente(aux->getKey()) : 0;
		}

		Studente* search(int pos){
			Node<Studente>* aux=_search(pos);
			return aux ? new Studente(aux->getKey()) : 0;
		}

};

int main(){

//esercizio 1

	int v=89, r=60, e=27;
	Studente *a=new Studente("X81000123", v, r, e);
	Studente *b=new Studente("X81000452", 63, 63, 24);
	Studente *c=new Studente("X81000104", 89, 60, 25);

	if(*a < *b){
		cout<<"True";
	}
	else{
		cout<<"False";
	}
	cout<<endl;

	if(*a >= *b){
		cout<<"True";
	}
	else{
		cout<<"False";
	}
	cout<<endl;

	if(*a == *c){
		cout<<"True";
	}
	else{
		cout<<"False";
	}
	cout<<endl;

	if(*a <= *c){
		cout<<"True";
	}
	else{
		cout<<"False";
	}
	cout<<endl;

//esercizio 2
	BST<int>* t=new BST<int>();
	t->ins(2)->ins(3)->ins(12)->ins(4)->ins(9)->ins(5);
	t->print();

	//cout<<*t->search(3);
	//cout<<t->search(10);

//esercizio 3
	Valutazione *val=new Valutazione();
	val->ins(*a)->ins(*b)->ins(*c);

	//cout<<*val->min();
	cout<<*val->search(2);
	val->print();

}