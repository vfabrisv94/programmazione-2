#include <iostream>
using namespace std;

template <class A> class Node{
	private:
			A key; int mul; 
			Node <A>* next, *prev;
	public:
			Node(A key) {
				this->key = key;
				mul = 1;
				next = prev = NULL;
			}
			
			Node() {
				next = prev = NULL;
				mul = 0;
			}
			
			void setNext(Node<A>* next){ this->next = next; }
			void setPrev(Node<A>* prev){ this->prev = prev; }
			void setKey(A key){ this->key = key; }
			void setMul(int mul){ this->mul = mul; }
			void incMul(){ this->mul++; }
			void decMul(){ this->mul--; }
			
			Node <A>* getNext(){ return this->next; }
			Node <A>* getPrev(){ return this->prev; }
			A getKey() { return this->key; }
			int getMul(){ return this->mul; }
};

template <class P> class MultiList {
		public:
		virtual MultiList<P>* ins(P x) = 0;
		virtual int multiplicity(P x) = 0;
		virtual void print() = 0;
};

template <class H> class MyMultiList: public MultiList <H>{
	private:
			Node <H>* header, *trailer;
			int size;
	public:
			MyMultiList() {
				header = new Node <H>();
				trailer = new Node <H>();
				header->setNext(trailer);
				trailer->setPrev(header);
				size = 0;
			}
			
			MyMultiList<H>* ins(H x) {
				Node <H>* newNode = new Node<H>(x);
				Node <H>* temp = header->getNext();
				
				while(temp != trailer) {
					if(temp->getKey() == x) {
						temp->incMul();
						size++;
						return this;
					}
					if(temp->getKey() > x) break;
					temp = temp->getNext();
				}
				
				temp->getPrev()->setNext(newNode);
				newNode->setNext(temp);
				newNode->setPrev(temp->getPrev());
				temp->setPrev(newNode);
				size++;
				return this;
			}
			
			void print(){
				Node <H>* temp = header->getNext();
				while(temp != trailer){
					for(int i = 0; i < temp->getMul();i++ )cout << temp->getKey() << " ";
					temp = temp->getNext();
				}
				cout << endl;
			}
			
			Node <H>* search(H x) {
				if(!size) return NULL;
				Node <H>* temp = header->getNext();
				while(temp != trailer) {
					if(temp->getKey() == x) return temp;
					temp = temp->getNext();
				}
				return NULL;
			}
			
			int multiplicity(H x) {
				Node <H>* temp = search(x);
				if(!temp) return 0;
				else return temp->getMul();
			}
			
			MyMultiList<H>* del(H x){
				Node <H>* temp = search(x);
				if(!temp || !size) return this;
				if(temp->getMul() > 1){
					temp->decMul();
					size--;
					return this;
				}
				size--;
				temp->getPrev()->setNext(temp->getNext());
				temp->getNext()->setPrev(temp->getPrev());
				return this;
			}
			
			int rank(H x){
				Node <H>* temp = search(x);
				if(!temp) return 0;
				temp = temp->getPrev();
				int rank = 0;
				while(temp != header) {
					for(int i =0; i < temp->getMul() ;i++)rank++;
					temp = temp->getPrev();
				}
				return rank+1;
			}
			
};

int main(){
		MyMultiList<int>* Listaccia = new MyMultiList<int>();
		int MyArray[] = {10, 5, 8, 3, 5, 1, 6, 5, 8, 1, 12, 7};
		for(int i = 0; i < 12;i++) 	Listaccia->ins(MyArray[i]);
		Listaccia->print();
		Listaccia->del(6)->del(5)->del(8)->del(3)->del(8);
		Listaccia->print();
		cout << Listaccia->rank(5) << endl;
		cout << Listaccia->rank(6) << endl;
		cout << Listaccia->rank(12) << endl;
		
}
