#include <iostream>
using namespace std;


template <class T>
class BST{
	public:
		virtual BST<T>* insert(T *x) = 0;
		virtual BST<T>* del(T *x)= 0;
		virtual int search(T *x) = 0;
		virtual void naturalFill(T*v) = 0;
		virtual void postorderPrint() = 0;
		virtual void printLevel(int l) = 0;
};
template <class T>
struct nodo{
	T val;
	nodo<T>*padre;
	nodo<T>*sx;
	nodo<T>*dx;
	};
template<class T> 
class mBST : public BST<T>{
	public:
		 mBST(){ this->root=NULL;};
		 ~mBST(){};
		 mBST<T>* insert(T *x) {
			 nodo<T>*nuovo=new nodo<T>;
			 nuovo->padre=NULL;
			 nuovo->dx=NULL;
			 nuovo->sx=NULL;
			 nuovo->val=*x;
			 if(this->root==NULL){
				 this->root =nuovo;
				 }else{
					 nodo<T>* app=this->root;
					 nodo<T>* app2;
					 while (app!=NULL){
						app2=app;
						 app->val< nuovo->val ? app=app->dx : app=app->sx;
						 }
						 app2->val <= nuovo->val ? app2->dx=nuovo : app2->sx = nuovo;
						 nuovo->padre=app2;
					 }
					return this;
			 };
		 mBST<T>* del(T *x) {};
		 int search(T *x) {
			nodo<T>*app=this->root;
			 while(app !=NULL && app->val != *x){
				 app->val < *x ? app =app->dx : app=app->sx;
				 }
					return (app==NULL ? 0 : 1) ;
			 } ;
			 void naturalFill(T*v) {
				 naturalFill( v, this->root);
				 };
			 void naturalFill(T*x,nodo<T>*current){
				static int p=0;
				if (current !=NULL && p<13){
					naturalFill(x,current->sx);
					current->val=*x+p;
					p=p+1;
					naturalFill(x,current->dx);
				} 
				};
		 void postorderPrint() {
			 postorder(this->root);
			 cout<<endl;
			 } ;
		 void postorder(nodo<T>* current){
			 if(current->sx !=NULL)
				 postorder(current->sx);
			 if(current->dx !=NULL)
				 postorder(current->dx);
			 cout<<current->val<<"	";
			 };
		void printLevel(int l ){
				int level =1;
					printLevel(l,this->root,0);
			};
			void printLevel(int l, nodo<T>*current, int level){
				
							if (level==l ){ cout<<"	"<<current->val; return; }
						level+=1;
							if(current->sx!=NULL)
						printLevel( l,current->sx,level);
							if(current->dx!=NULL)
						printLevel ( l,current->dx,level);
						
				 	
			};
		 nodo<T>*root;
	};
	
	
	
	int main(){
		int dainserire[]={23,4,6,8,12,21,5,9,7,3,16,2,24};
		int natfill[]={1,2,3,4,5,6,7,8,9,10,11,12,13};
		mBST<int>*mio=new mBST<int>();
		
		for(int i=0;i<13;i++)
			mio->insert(&dainserire[i]);
		cout<<"Primo inserimento (postorder print)"<<endl;
		mio->postorderPrint();
		cout<<"Print Level : 3 "<<endl;
		mio->printLevel(3);
		cout<<endl;
		mio->naturalFill(natfill);
		cout<<"Natural Fill"<<endl;
		mio->postorderPrint();
		
		
		
		
		return 8;
		
		
		}
