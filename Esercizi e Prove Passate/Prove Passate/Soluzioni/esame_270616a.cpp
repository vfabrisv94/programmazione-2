#include <iostream>

using namespace std;

template <class H> class Node{
	
	private:
		H* key;
		Node<H> *next, *prev;

	public:
		Node(H* k=0, Node<H> *n=0, Node<H> *p=0) : key(new H(*k)), next(n), prev(p) {}

		void setKey(H *k){
			key=k;
		}

		void setNext(Node<H> *n){
			next=n;
		}

		void setPrev(Node<H> *p){
			prev=p;
		}

		H* getKey() const{
			return key;
		}

		Node<H>* getNext() const{
			return next;
		}

		Node<H>* getPrev() const{
			return prev;
		}
};


template <class H> class List{
	
	protected:
		Node<H> *head, *tail, *current;

		Node<H>* _search(H x){
			Node<H>* aux=head;
			if(!aux){
				return 0;
			}
			while(aux){
				if(x == *aux->getKey()){
					return aux;
				}
				aux=aux->getNext();
			}
			return 0;
		}

	public:
		List(){
			head=tail=0;
			current=0;
		}

		List<H>* ins(H x){
			if(!head){
				head=new Node<H>(&x);
				tail=head;
				return this;
			}

			//inserimento in testa
			if(x < *head->getKey()){
				Node<H>* aux=new Node<H>(&x, head);
				head->setPrev(aux);
				head=aux;
				return this;
			}

			//inserimento in coda
			if(x > *tail->getKey()){
				Node<H>* aux=new Node<H>(&x, 0, tail);
				tail->setNext(aux);
				tail=aux;
				return this;
			}

			//inserimento standard
			Node<H>* current=head->getNext();
			while(current && x >= *current->getKey()){
				current=current->getNext();
			}
			if(current){
				Node<H>* aux=new Node<H>(&x, current, current->getPrev());
				aux->getPrev()->setNext(aux);
				current->setPrev(aux);
			}
			return this;
		}

		int search(H x){
			Node<H>* aux=_search(x);
			return aux ? 1 : 0;
		}

		List<H>* del(H x){
			Node<H>* aux=_search(x);

			if(!aux){
				return this;
			}

			//eliminazione in testa
			if(aux == head){
				head=head->getNext();
				head->setPrev(0);
				delete aux;
				return this;
			}

			//eliminazione in coda
			if(aux == tail){
				tail=tail->getPrev();
				tail->setNext(0);
				delete aux;
				return this;
			}

			//eliminazione standard
			Node<H>* back=aux->getPrev();
			Node<H>* forward=aux->getNext();
			back->setNext(forward);
			forward->setPrev(back);
			delete aux;
			return this;
		}

		Node<H>* getHead() const{
			return head;
		}

		void print(){
			Node<H>* aux=head;
			while(aux){
				cout<<*aux->getKey()<<" ";
				aux=aux->getNext();
			}
			cout<<endl;
		}
		
		void del2(Node<H>* aux){
			aux=aux->getNext();
			head=aux;
			if(!head){
				tail=0;
			}
		} 

};

template <class H> class Queue : public List<H>{
	
	private:
		Node<H> *front, *rear;
		int n;

		Node<H>* _search(H x){
			Node<H>* aux=front;

			while(aux){
				if(x == *aux->getKey()){
					return aux;
				}
				aux=aux->getNext();
			}
			return 0;
		}


	public:
		Queue(){
			List<H>();
			front=rear=0;
			n=0;
		}

		int getN() const{
			return n;
		}

		H getSum() const{
			if(!front){
				return -9999;
			}

			H sum=0;
			Node<H>* aux=front;
			while(aux){
				sum+=*aux->getKey();
				aux=aux->getNext();
			}
			return sum;
		}

		H* getFront() const{
			return front ? new H(*front->getKey()) : 0;
		}

		bool isEmpty(){
			if(!front){
				return true;
			}
			return false;
		}

		Queue<H>* push(H x){
			if(!front){
				front=new Node<H>(&x, front);
				rear=front;
				n++;
				return this;
			}

			Node<H>* aux=new Node<H>(&x);
			rear->setNext(aux);
			rear=aux;
			n++;
			return this;
		}

		H* pop(){
			if(!front){
				return 0;
			}

			Node<H>* aux=front;
			front=aux->getNext();
			aux->setNext(0);
			n--;
			H* tmp=new H(*aux->getKey());
			delete aux;
			return tmp;
		}

		int search(H x){
			Node<H>* aux=_search(x);
			return aux ? 1 : 0;
		}

		void print(){
			Node<H>* aux=front;
			while(aux){
				cout<<*aux->getKey()<<" ";
				aux=aux->getNext();
			}
			cout<<endl;
		}

		friend bool operator > (const Queue<H> &c1, const Queue<H> &c2){
			if(c1.getSum() > c2.getSum()){
				return true;
			}
			if(c1.getSum() == c2.getSum() && c1.getN() > c2.getN()){
				return true;
			}
			if(c1.getSum() == c2.getSum() && c1.getN() == c2.getN() && *c1.getFront() > *c2.getFront()){
				return true;
			}
			return false;
		}

		friend bool operator < (const Queue<H> &c1, const Queue<H> &c2){
			return c2>c1;
		}

		friend bool operator >= (const Queue<H> &c1, const Queue<H> &c2){
			return !(c1<c2);
		}

		friend bool operator == (const Queue<H> &c1, const Queue<H> &c2){
			return !(c1<c2) && !(c1>c2);
		}


};

template <class H> class BrikList{
	
	public:
		virtual BrikList<H>* ins(H x) = 0;
		virtual BrikList<H>* push(H x) = 0;
		virtual H* pop() = 0;
		virtual int search(H x) = 0;
		virtual void print() = 0;
};

template <class H> class MyBrikList : public BrikList<H>{
	
	private:
		List< Queue<H> >* l;
		int elem;

	public:
		MyBrikList(){
			l=new List< Queue<H> >();
			elem=0;
		}

		BrikList<H>* ins(H x){
			Queue<H>* c=new Queue<H>();
			c->push(x);
			l->ins(*c);
			elem++;
			return this;
		}

		BrikList<H>* push(H x){
			Node< Queue<H> >* head=l->getHead();
			if(!head){
				return this;
			}
			head->getKey()->push(x);
			Queue<H>* tmp=head->getKey();
			l->del(*head->getKey());
			l->ins(*tmp);
			return this;
		}

		H* pop(){
			Node< Queue<H> >* head=l->getHead();
			H* val=head->getKey()->pop();
			if(head->getKey()->getN()==0){
				l->del2(head);
				elem--;
			}
			cout<<endl;
			return val;
		}

		int search(H x){
			Node< Queue<H> >* head=l->getHead();
			int result=0;
			while(head){
				result=head->getKey()->search(x);
				if(result==1){
					return 1;
				}
				head=head->getNext();
			}
			return 0;
		}

		void print(){
			Node< Queue<H> >* aux=l->getHead();
			while(aux){
				aux->getKey()->print();
				aux=aux->getNext();
			}
			cout<<endl;
		}
};


int main(){
//	List<int>* l=new List<int>();
//	l->ins(10)->ins(11)->ins(1)->ins(5)->ins(4)->ins(3);
//	l->print();
//	cout<<l->search(11);
//	cout<<l->search(7);
//	cout<<endl;
//	l->del(1)->del(11)->del(4)->del(2)->del(12);
//	l->print();
//	
//	cout<<endl;
//
//	Queue<int>* q=new Queue<int>();
//	q->push(7)->push(4)->push(12)->push(9)->push(22);
//	q->print();
//	q->pop();//->pop()->push(41)->push(4)->pop();
//	q->print();
//	cout<<"n: "<<q->getN()<<"\t sum: "<<q->getSum()<<"\t front: "<<*q->getFront()<<endl;
//	cout<<q->search(11);
//	cout<<q->search(4);
//	cout<<endl;
//
//	cout<<endl;

	MyBrikList<int>* bl=new MyBrikList<int>();
	bl->ins(15)->ins(12)->ins(6)->ins(9)->ins(10)->ins(4)->ins(2)->ins(30)->ins(23)->ins(11);
	bl->print();
	cout<<endl;
	/*bl->push(99);
	bl->print();
	cout<<endl;
	bl->push(100);
	bl->print();
	cout<<endl;
	bl->push(98);
	bl->print();
	cout<<endl;
	cout<<bl->search(12)<<"\t";
	cout<<bl->search(13)<<endl;
	cout<<"pop: "<<*bl->pop()<<endl;
	cout<<endl;
	bl->print();
	cout<<"pop: "<<*bl->pop()<<endl;
	cout<<endl;
	bl->print();
	cout<<"pop: "<<*bl->pop()<<endl;
	bl->print();
	cout<<"pop: "<<*bl->pop()<<endl;
	bl->print();*/

	for(int i=0; i<4; i++){
		int* val=bl->pop();
		bl->push(*val);
	}
	bl->print();
}
