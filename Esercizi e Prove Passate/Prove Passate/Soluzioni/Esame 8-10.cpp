#include <iostream>
using namespace std;

template <class A> class Node {
	private:
			Node <A>* parent, * left,* right;
			int mul; A key;
	public:
			Node(A key) {
				parent = right = left = NULL;
				this->key = key;
				mul = 1;
			} 
			void setKey(A key){ this->key = key; }
			void setParent(Node <A>* parent){ this->parent = parent; }
			void setLeft(Node <A>* left){this->left = left;}
			void setRight(Node <A>* right){ this->right = right;}
			void incMul(){ this->mul++; }
			void decMul(){ this->mul--; }
			
			Node <A>* getParent(){ return this->parent; }
			Node <A>* getLeft(){ return this->left;}
			Node <A>* getRight(){ return this->right; }
			A getKey(){ return this->key;}
			int getMul(){ return this->mul; }
			bool isLeaf(){ return !getRight() && !getLeft()? true : false; }
};
template <class H> class MultiBST {
	public:
		virtual MultiBST<H>* ins(H x) = 0;
		virtual int search(H x) = 0;
		virtual void inorder() = 0;
		virtual MultiBST<H>* del(H x) = 0;
};

template <class Q> class MyMultiBST: MultiBST <Q> {
	private:
			Node <Q>* root; 
			int size;
			
			void _inorder(Node <Q>* temp) {
				if(!temp) return;
				_inorder(temp->getLeft());
				for(int i = 0; i < temp->getMul();i++) cout << temp->getKey() << " ";
				_inorder(temp->getRight());
			}
	public:
			MyMultiBST(){
				root = NULL;
				size  = 0;
			}
			
			MultiBST <Q>*ins(Q x) {
				Node <Q>* temp = root;
				Node <Q>* father = NULL;
				
				while(temp != NULL){
					father = temp;
					if(temp->getKey() == x) {
						size++;
						temp->incMul();
						return this;
					}
					if(temp->getKey() > x) temp = temp->getLeft();
					else temp = temp->getRight();
				}
				
				Node <Q>* newNode = new Node<Q>(x);
				if(!root) root = newNode;
				else {
					if(father->getKey() > x) father->setLeft(newNode);
					else father->setRight(newNode);
					newNode->setParent(father);
				}
				size++;
				return this;
			}
			
			void inorder(){
				_inorder(root);
				cout << endl;
			}
			
			Node <Q>* _search(Node <Q>* temp, Q x){
				if(!temp) return NULL;
				if(temp->getKey() == x) return temp;
				if(temp->getKey() > x) return _search(temp->getLeft(),x);
				else return _search(temp->getRight(),x); 
			}
			
			int search(Q x){
				cout<< x << " is: ";
				cout << ((_search(root,x))? 1 : 0);
				cout <<  " ";
				cout << endl;
				return (!_search(root,x))? 0 : 1;
			}
			
			int multiplicity(Q x){
				Node <Q>* temp = _search(root,x);
				if(!temp) return 0;
				else return temp->getMul();
			}
			
			MultiBST<Q>* _del(Node <Q>* r, Q x) {
				Node <Q>* temp = _search(r,x);
				if(!temp) return this;
				if(temp->getMul() > 1){
					temp->decMul();
					size--;
					return this;
				}
				Node <Q>* father = temp->getParent();
				if(temp->isLeaf()){
					size--;
					if(!father) { root = NULL; return this; }
					else if(father->getRight() == temp) father->setRight(NULL);
					else father->setLeft(NULL);
					return this;
				}
				Node <Q>* son = NULL;
				if(!temp->getRight() || !temp->getLeft()) {
					size--;
					if(!temp->getRight()) son = temp->getLeft();
					else son = temp->getRight();
					
					if(!father) root = son;
					else if(father->getRight() == temp) father->setRight(son);
					else father->setLeft(son); 
					return this;
				}
				
				Node <Q>* next = getNext(temp);
				temp->setKey(next->getKey());
				_del(temp->getRight(),next->getKey());
				return this;
			}
			
			Node <Q>* getMin(Node <Q>* temp){
				if(!temp) return NULL;
				while(temp->getLeft() != NULL) temp = temp->getLeft();
				return temp;
			}
			
			Node <Q>* getMax(Node <Q>* temp) {
				if(!temp) return NULL;
				while(temp->getRight() != NULL) temp = temp->getRight();
				return temp;
			}
			
			Node <Q>* getNext(Node <Q>* temp) {
				if(!temp) return NULL;
				if(temp == getMax(root)) return NULL;
				if(temp->getRight()) return getMin(temp->getRight());
				while(temp->getParent()->getKey() <= temp->getKey()) temp = temp->getParent();
				return temp->getParent(); 
			}
			
			MultiBST<Q>* del(Q x) {
				return _del(root,x);
			}
			
			int rank(Q x){
				Node <Q>* temp = _search(root,x);
				if(!temp) return 0;
				int rank = 1;
				Node <Q>* min = getMin(root);
				while(min != NULL) {
					if(temp->getKey() > min->getKey() ) rank++;
					min = getNext(min);
				}
				return rank+1;
			}
}; 


int main(){
	MyMultiBST<int>* Multipletta = new MyMultiBST<int>();
	Multipletta->ins(10)->ins(7)->ins(7)->ins(23)->ins(30)->ins(4)->ins(1)->ins(5)->ins(9)->ins(5)->ins(1)->ins(7)->ins(1)->ins(9);
//	10, 7, 7, 23, 30, 4, 1, 5, 9, 5, 1, 7, 1, 9
//  1, 1, 1, 4, 5, 5, 7, 7, 7, 9, 9, 10, 23, 30
	/*
	Multipletta->inorder();
	Multipletta->search(4);
	Multipletta->search(9);
	Multipletta->search(23);
	Multipletta->search(50);
	cout << Multipletta->multiplicity(1);
	cout << endl;
	cout << Multipletta->multiplicity(7);
	cout << endl;
	cout << Multipletta->multiplicity(4);
	cout << endl;
	*/
	/*
	Multipletta->inorder();
	Multipletta->del(7)->del(4)->del(23)->del(7)->del(7);
	Multipletta->inorder();
	*/
	cout << Multipletta->rank(5) << endl;
	cout << Multipletta->rank(9) << endl;
	cout << Multipletta->rank(30) << endl;
	
}
