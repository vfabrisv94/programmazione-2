#include <iostream>

using namespace std;

class Contatore{
	public:
		Contatore(){
			conto=1;
		}

		void incrementa(){
			conto++;
		}

		int leggi_conto(){
			return conto;
		}

	private:
		unsigned int conto;


};

int main(){
	Contatore c1,c2;
	cout<<"c1 : "<<c1.leggi_conto()<< "\nc2 : "<<c2.leggi_conto()<<endl;
	c1.incrementa();
	c2.incrementa();
	cout<<"c1 : "<<c1.leggi_conto()<< "\nc2 : "<<c2.leggi_conto()<<endl;
	return 0;
}