/*TCap13_3.cpp. Operatori da ridefinire: +,++,--,=,==(se c'è tempo),>>. Importante gli operatori incui bisogna ridefinire per quando si 
deve lavorare con la memoria dinamica ,in modo da fare cose come somme di due liste*/
/*NOTE TEORIA:
0)la ridefinizione degli operatori ha senso quando si fanno operazioni su tipi definiti dall'utente o nuovi tipi predefiniti. Ovviamente se
l'operatore è binario,la ridefinizione ha senso anche quando uno dei due operandi è un tipo predefinito(int,double ,ecc) e l'altro no.Quindi
REGOLA: ALMENO uno degli argomenti della funzione operatore ( gli argomenti saranno uno o max due per operatori binario)deve essere un oggetto
di una classe o un riferimento ad essa. 
1)Le funzioni operatori possono essere funzioni membro o funzioni globali (rese friend). Il primo caso si ha quando abbiamo la necessità che
l'operando sinistro sia un oggetto della classe in cui abbiamo definito la funzione operatore.Il secondo caso quando come operando sinistro
c'è un tipo fondamentale (Ex int o double)o un oggetto diverso da quello in cui appartiene la funzione operatore. 
2)I seguenti operatori: ->,(),[], =  ed ogni altro operatore di assegnamento deve essere per forza definito con una funzione MEMBRO.
3) Gli operatori >> e << devono essere ridefiniti per forza come funzioni globali . Del resto se consideriamo <<Oggetto ,l'operatore "<<" 
ha come operando sinistro un oggetto di tipo Ostream e quindi "<<"dovrebbe essere un membro della classe OSTREAM ,ma cioè non è possibile perchè
non si possono modificare le classi della libreria standard del c++.
4) Infine se si vogliono fare operazioni commutative ex . Oggetto = Oggettoa + Oggettob e Oggetto = Oggettobb+Oggettoba. Ci sarà un caso 
in cui a sinistra si trova un oggetto diverso da quello in cui è definita la funzione operatore. Quindi in tal caso bisogna renderla globale.
5)Tutti gli operatori ,tranne 3 ,per operare su oggetti di classe DEVONO essere ridefiniti. Tranne " = ","&","," .Difatti è possibile
fare senza alcuna ridefinizione : Oggetto1=Oggetto2. Oppure Oggetto1= &Oggetto2. Tali operatori conviene però ridefinire quando si lavora
con la memoria dinamica.
6)Per la ridefinizione si deve sempre costrutire una funzione in cui compare la parola "operator"seguita dall'operatore da ridefinire */
/*NOTE ESERCIZIO: ridefinizione operatori di inserimento ed estrazione,ovvero >> e <<.  Per il punto 3) devono essere ridefiniti come funzioni
globali o eventualmente friend di una certa classe ,ma comunque non membro.*/
#include<iostream>
#include<string>
#include<iomanip>
using namespace std;


class PhoneNumber
{
	//entrambe le funzioni restituiscono un riferimento per fare in modo che vengano effettuate operazioni in cascata
	friend ostream& operator<<(ostream &,const PhoneNumber&); // a sinistra di << troviamo un oggetto di OuputStream e a destra un oggetto di PhoneNumbero.Siccome a sinistra non troviamo un oggetto della nostra lcasse,la funzione è friend
	friend istream& operator>>(istream&, PhoneNumber&); //non const perchè deve essere modificato il nostro oggetto
	friend void prova(PhoneNumber &);
	
	private:
		string areaCode;//prefisso 3 cifre
		string exchange; //prime  3 cifre del numero
		string line; //ultime 4 cifre del numero
	public:
		string getArea() {return areaCode;}
};

void prova (PhoneNumber & obj)
{

	obj.areaCode="Ale";
}
ostream& operator<<(ostream& output,const PhoneNumber &number) //output della classe Ostream è l'operando a sinistra di <<
{
	output<< "("<<number.areaCode <<" ) " <<number.exchange <<" -" <<number.line;
	//output<<"Ale" <<number.areaCode;
	
	//posso accedere agli oggetti private di PhoneNumber in quanto la funzione è friend
	return output;

}
istream& operator >>(istream&input,PhoneNumber &number)
{
	input.ignore();//salta un carattere . In questo caso '('
	input>>setw(3)>>number.areaCode ; //specifica che a number.areaCode verranno assegnati solo 3 numeri
	input.ignore(2); //salta la parentesi chiusa e gli spazi
	input>>setw(3) >>number.exchange;
	input.ignore();
	input>>setw(4)>>number.line;//input parola restante*/
	return input;
}

int main(){
cout<<"Ridefinizione operatori"<<endl;

PhoneNumber phone;
prova(phone);
cout<<phone.getArea()<<endl;

cout<<"Enter Phone number in the form (123) 456-7890:" <<endl;
cin>>phone ;
//invoca l'operatore ridefinito in quanto opera su un oggetto  con la chiamata della funzione operator>>(cin,phone)
//cin>>phone;
cout<<"The phone numbero entered was: ";
cout<<phone <<endl;

return 0;
}
