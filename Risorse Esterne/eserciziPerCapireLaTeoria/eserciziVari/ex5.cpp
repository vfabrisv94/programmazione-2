#include<iostream>
#include<string>
/*Algoritmo PER CONVERTIRE UN'ESPRESSIONE INFIX IN POST FIX  DEL TIPO : ( 6+ 2) *5 - 8  / 4 diventa : 6 2 + 5* 8 4 / - . L'algoritmo è 
il seguente : 
inserisci prima di tutto , una parentesi aperta nella pila e una chiusa nell'array infix che contiene la stringa di input 
leggo i caratteri dell'array infix : 
1) se trovo un numero lo inserisco nell'array di output postfix 
2) se trovo un operatore estraggo dalla pila tutti gli operatori con priorità maggiore o uguale e li inserisco nell'array di output 
mentre l'operatore attuale lo inserisco nella pila . 
3) se trovo una parentesi chiusa,estraggo dalla pilla tuttli gli operatori che verranno inseriti nell'array di output,ifin qauando non 
incontro una parentesi.*/
using namespace std;
template<typename T>
class Node
{
	public:
		Node( const T & input=' '):data(input),prevPtr(0),nextPtr(0){}
		Node<T>* getNextPtr()const {return nextPtr;}
		void setNextPtr(Node<T> *const p ){ nextPtr = p ;}
		Node<T> *getPrevPtr()const {return prevPtr;}
		void setPrevPtr( Node<T> *const p  ){prevPtr = p ;}
		T getData()const {return data;}
		void setData( const char & input){ data = input;}
	private:
		Node<T>* prevPtr,*nextPtr;
		T data;
};
template<typename T>
class Stack
{
	public:
		Stack():head(0){}
		void push( const char & i )
		{ 
			if(isEmpty())
				head = new Node<T>( i );
			else 
				{
					Node<T>* newNode = new Node<T>(i);
					newNode->setNextPtr(head);
					head->setPrevPtr(newNode);
					head = newNode;
				}
		}
		char pop()
		{
			
			Node<T>*toDelete = head;
			char i = head->getData();
			head = head->getNextPtr();
			if(head)head->setPrevPtr(0);
			toDelete->setNextPtr(0);delete toDelete;toDelete = 0;
			return i ;
		}
		void print()
		{
			if(isEmpty())
			{
				cout<<"\nisEmpty!\n";
			}else
				{	
					Node<T>*index = head;
					while(index!=NULL)
					{
						cout<<index->getData()<<endl;
						index = index->getNextPtr();
					}
				
				}
		}
		char getTop()const
		{
			if(isEmpty())return 0;
			return head->getData();
		}
		bool isEmpty()const {return head==0;}
	private:
		Node<T>*head;
};
template<typename T>
class Postfix
{
	public:
		Postfix(){ stack = new Stack<T>();}
		string convertToPostfix( string infix )
		{
			cout<<" Espressione infix : "<<infix<<endl;
			string input = infix ;  input+=')';
			int n = input.length();
			string output="";
			push('(');
			int x =0;
			while( isEmpty()==false && x<n)
			{
				char act = input[ x ];
				if( act=='(' ) push( act);
				else if ( act==')' )
				{
					char p =pop();
					while(p!=0 && p!='(')
					{
						output+=p;
						p = pop();
					}
				}else if(isOperator(act))
				{
					while(getTop()!=0 && isOperator( getTop() )==true && precedence(act, getTop() )!=1 )
					{
						output+=pop();
					}
						push(act);
				}else // è un numero  
					{
						output+=act;
					}
			 x++;
			}
			
			return output;
		}
		
	protected:
		Stack<T> * stack ; 
		void reset()
		{
			while(stack->isEmpty()==false)
			stack->pop();
		}
		bool isOperator(char i )
		{
			if( i=='*' || i=='/'|| i =='+' || i=='-' )return true;
			return false;
		}
		bool isMulOrDiv( char op)
		{
			if(op=='*' || op=='/' )return true;
			return false;
		}
		int precedence( char operator1 ,char operator2 ) //-1 se la prece. di operator1 è < ,0 uguale,1 >
		{
			if(operator1==operator2)return 0;
			bool first = isMulOrDiv(operator1);
			bool second = isMulOrDiv(operator2);
			if(first==second)return 0 ; 
			if(first==false)return -1;
			return 1;
		}
		void push( char data)
		{
			stack->push(data);
		}
		char pop()
		{
			return stack->pop();
		}
		char getTop()
		{
			return stack->getTop();
		}
		bool isEmpty()const 
		{
			return stack->isEmpty();
		}
		void printStack()
		{
			stack->print();
		}
		
};
/* Algoritmo per valutare un'espressione postFix : 
0) si prenda in input una stringa che rappresenta una espressione postFix e si aggiunge il carattere di terminazione \0 
1) si legge la stringa del punto precedente : 
a) se leggo un numero,lo inserisco in una pila 
b) se leggo un operatore estraggo i primi due elementi dalla pila e calcolo il secondo elemento estrattore operatore primo elemento estratto.
Rimpongo il risultato nella pila 
*/
template<typename T>
class ValuePostfix  : public Postfix<T>
{
	public:
		ValuePostfix(){}
		bool isNum( char n )
		{
			 for(int i=0;i<=9 ;i++)
			 	if( n -'0' ==i )return true;
			 return false;
		}
		int evalutePostfixExpression( string input)
		{
			string postFix = input ; postFix+='\0';//determina la fine della lettura
			int x =0;
			char act = postFix[ x ];
			while( act!='\0')
			{
				if( isNum(act) )
				{
					Postfix<T>::push( act -'0' );
				}else 
					{	
						int second = Postfix<T>::pop();
						int first = Postfix<T>::pop();
						int result = calculate(first,act,second);
						Postfix<T>::push( result);
					}
				x++;
				act = postFix[  x ];
			}
			int r = Postfix<T>::pop();
			Postfix<T>::reset();
			return r;
		}
		
	private:
		int calculate( int op1 ,char operator_,int op2 )
		{
			int result = 0 ; 
			if(operator_=='*')result = op1*op2;
			else if(operator_=='/' ) result = op1/op2;
			else if(operator_=='-')	result=op1-op2;
			else if(operator_=='%') result = op1%op2;
			else if(operator_=='^') result=op1^op2 ;
			else result = op1+op2;
			return result ;
		}
};
int main()
{	
	string  str ="(6+2)*5-8/4"; //5-8/4
	Postfix<char>*post = new Postfix<char>();
	string postFix = post->convertToPostfix( str);
	cout<<postFix <<endl;
	ValuePostfix<int>*valuePost = new ValuePostfix<int>( );
	int r = valuePost->evalutePostfixExpression( postFix );
	cout<<" risultato : "<< r <<endl;

}