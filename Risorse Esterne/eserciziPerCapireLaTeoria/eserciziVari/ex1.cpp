/*costruzione di una lista di array statici e loro gestione più efficace*/
#include<iostream>
const int k = 3;
using namespace std;
template<typename T>
class Array
{
	public:
		T& operator[]( int subscript);
		Array(){ 
		for(T i =0; i<k+1;i++)
			container[ i ] = i+10;
			numEle =0;
			toMove = false;
		}	
		void increaseNum(){ numEle++;}
		void decreaseNum(){numEle--; }
		void moveToLeft(int index) ;//sposta gli elementi alla posizione index per eliminare un elemento
	void print() const
		{
			cout<<"[ ";
			for(int i=0; i<numEle;i++)
				cout<<container [ i ]<<" ";
			cout<<" ] \n";
		}
		void moveToRight( int index); //spota gli elmenti di un posto a destra per fare spazio ad un nuovo elemento
	int getNumEle()const {return  numEle;}
	bool isToMove()const {return toMove==true ;}
	void removeToMoveIndex() { toMove = false;}
	protected:
		T container[k+1];
		int numEle; //elementi attuali
		bool toMove;
};
template<typename T>
void Array<T>::moveToLeft(int index)
{
	int Move = numEle - (index+1) ; //elementi da spostare 
	for(int i = 1 ; i<=Move && index+1<k; i++)
	{
		container[ index ] = container[ index+1 ];
		index++;
	}
}
template<typename T>
void Array<T>::moveToRight(int index)
{
	int Move = numEle - (index) ; //elementi da spostare  . Si noti che rispetto a moveToLeft non si ha "index+1".Questo perchè non stiamo eliminado un elemento quindi il numero di elementi da spostare include anche l'elemento che si trova nella posizione in cui verrà piazzato il nuovo
	int last = numEle;
	for(int i = 1 ; i<=Move && last!=index; i++)
	{
		container[ last ] = container[ numEle-i ];
		last--;
	}
	if(numEle == k ) toMove = true; //significa che avevevamo un array già pieno e che dopo l'inserimento avremo un elemento nella posizione di riserva che dovrà essere spostato
	else
		numEle++;//l'array non ddddera pieno al max . Quindi si aggiorna il numero di elementi
}
template<typename T>
T& Array<T>::operator[]( int subscript) 
{
	if ( subscript <0 || subscript >k )
	{
		cout<<"Out of range"<<endl;
		exit(1);
	}
	return this->container[ subscript ] ;
}
template<typename T>
class Node
{
	public:
		Node(T *input=NULL ):nextPtr(0),prevPtr(0){  data =  input==NULL ? 0 : new T(*input);}
		Node<T>*getNextPtr()const {return nextPtr;}
		void setNextPtr( Node<T>*const p ){nextPtr = p;}
		Node<T>*getPrevPtr()const {return prevPtr;}
		void setPrevPtr( Node<T>*const p ){prevPtr = p;}
		T* getData() const { return data;}
		void setData( T * i ){data= i ;}
	private:
		Node<T>*prevPtr,*nextPtr;
		T* data;
};
template<typename T>
class List
{
	public:
	List():head(0),tail(0){}
	void insertAtBack(  T x ) ;
	void insertAtFront(T x);
	void removeFromBack();
	Node<T>*getHead() const {return head;}
	void setHead( Node<T>*p ){head = p;}
	Node<T>*getTail() const {return tail;}
	void setTail( Node<T>*p ){tail = p;}
	protected:
		Node<T>*head,*tail;
		bool isEmpty(){ return head==0;}
};
template<typename T>
void List<T>::removeFromBack()
{
	if(isEmpty() )return;
	Node<T>*toDelete = tail;
	tail = toDelete->getPrevPtr();
	toDelete->setPrevPtr(0);
	if(tail)
		tail->setNextPtr(0);
	delete toDelete ;toDelete =0;
}
template<typename T>
void List<T>::insertAtBack( T x)
{	
	if(isEmpty() )
		List<T>::insertAtFront( x);
	else
		{
			Node<T>*newNode = new Node<T>( &x);
			tail->setNextPtr(newNode);
			newNode->setPrevPtr( tail);
			tail = newNode;
		}
}
template<typename T>
void List<T>::insertAtFront( T x)
{	
	if(isEmpty() )
		head=tail = new Node<T>(&x);
	else
		{
			Node<T>*newNode = new Node<T>( &x);
			head->setPrevPtr(newNode);
			newNode->setNextPtr( head);
			head = newNode;
		}
}
template<typename T>
class ListArray
{	
	public:
		ListArray(){  list = new List< Array<T> > ; }
		ListArray<T>* insert( T  x);
		ListArray<T>* insertInOrd( T x);
		void remove( T x);
		void print() ;
	private:
		List<Array<T> >*list;
		bool isEmpty()const { return list->getHead()==0;}
};
template<typename T>
ListArray<T>* ListArray<T>::insertInOrd(T x )
{
	if(isEmpty() ) { ListArray<T>::insert( x); return this; }
	//trova array in cui inserire la x
	Node< Array<T> >* index = list->getHead();
	bool stop = false;
	int i =0;
	T key = x;
	while ( index!=NULL && stop ==false )
	{
		int numAct = index->getData()->getNumEle();
		Array<T>*p = index->getData();
		i=0;
		if ( ( (*p)[0]<key && (*p)[ k-1 ]>key  ) || (*p)[0]>key|| numAct<k )
			{
			while( i<numAct && (*p)[i]<key ) //cerco indice dove piazzare l'elemento
					i++;
				if(i==numAct && numAct<k ) //unico caso in cui il while sopra si blocca anche quando non trova un elemento < x. Cioè quando il numero di elementi dell'array è comunque minore rispetto alla dimensione max.
				{	(*p)[ numAct ] = key;
					p->increaseNum(); //ci sono spazi vuoti quindi aumenta
					stop =true;
				}else
				{	
					p->moveToRight(i); //altrimenti piazzo l'elemento in modo ordinato spostando gli altri di un posto a destra
					(*p)[ i ] = key;
				}
				if(p->isToMove() ==false ) //significa che la posizione di riserva non è stata usata quindi comunque l'array ha al più k elementi 
					 stop= true;
				else
					{ //significa che è avanzato un elemento nella posizione di riseva. Tale elmento andrà messo negli array successivi
						key  = (*p)[k]; //la nuova chiave è il nuovo elemento
						p->removeToMoveIndex(); //resetta posizione di riserva
					}
			} // se non si entra nell'if significa che l'elemento deve essere inserito per forza dopo. 
			index = index->getNextPtr();
	}
	if(stop==false)  // se stop è false significa che è avanzato un elemento e quindi gli array precedenti sono pieni. Se invece fosse true significherebbe che abbiamo concluso l'operazione . In tal caso index sarà null se la lista è stata usata tutta ,non null se abbiamo piazzato l'elemento e qualche array successivo aveva posizioni vuote
		ListArray<T>::insert( key); //inserisci in coda 
return this;
}
template<typename T>
void ListArray<T>::remove( T x)
{
	if(isEmpty())return;
	
	Node< Array<T> >* index = list->getHead();
	bool find = false;
	int i=0; //posizione elemneto da eliminare
	while(index!=NULL && find==false ) //trova l'array dove troviamo l'elemento da eliminare
	{
		
		int size = index->getData()->getNumEle();
		i =0;
		while ( i<size && find==false)
		{
			if( (*index->getData() )[ i ]==x ) 
				find= true;
			else
				i++;
		}
		if(find==false)
			index=index->getNextPtr();
	}
	if(index==NULL) return; //nessun eleemento trovato
	
	while(index!=NULL) //dovremo scorrere tutti gli array 
	{
		Array<T>*act = (index->getData() );
		act->moveToLeft(i); //sposta gli lementi a sinistra
		act->decreaseNum();// il numero di elementi diminuisce . A meno che non ripristiniamo il numero aggiungendo elementi di array successivi

		if( index->getNextPtr()!=NULL ) // se è vero possiamo prendere gli elementi degli array successivi e aggiungerli a quello attuale
		{	
			Array<T>*next = (index->getNextPtr()->getData()  );
			(*act)[ k-1 ] = (*next)[0]; // prendiamo SEMPRE il primo elemento dell'array successivo
			act->increaseNum();//riaggiorniamo numero 
		}
		i =0;//d'ora in poi si sposteranno gli elementi nella posizione 0 
		index =index->getNextPtr();
	}
	if(list->getTail()->getData()->getNumEle()==0) // l'ultimo array o ha elementi o è vuoto. In tal caso si elimina
		list->removeFromBack();
}
template<typename T>
ListArray<T>*  ListArray<T>::insert(T x )
{
	Node< Array<T> >* index = list->getHead();
	while(index!=NULL && index->getData()->getNumEle()==k )
		index = index->getNextPtr();
	if(index==NULL)
	{
			Array<T>*newArray = new Array<T>();
			(*newArray)[ 0 ] = x ;
			newArray->increaseNum();
			list->insertAtBack( *newArray);
	}else
		{
			int act = index->getData()->getNumEle();
			(*index->getData())[ act ] = x ;
			index->getData()->increaseNum();
		}
		
	return this;
}
template<typename T>
void ListArray<T>::print()
{
	if(isEmpty())return;
	Node< Array<T> >* index = list->getHead();
	while(index!=NULL)
	{
		index->getData()->print();
		index = index->getNextPtr();
	}
}
int main()
{
ListArray<int>* al = new ListArray<int>();
al->insertInOrd(  69)->insertInOrd(61)->insertInOrd(68)->insertInOrd(65)->insertInOrd(66)->print();
return 0;
}