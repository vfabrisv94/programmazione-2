#include<iostream>
using namespace std;
/*Algoritmo per creare alberi di espressione algebriche,cioè alberi che rappresentano delle espressioni. L'algoritmo è il seguente:
0) si suppone che ogni operatore ad eccezione del più esterno sia compreso tra i due operando e questi a sua volta,dentro una parentesi
1) la prima volta che si incontra una parentesi aperta ,creare un nodo nuovo che sarà radice. Assegnare il nodo attuale a tale nodo
2) ogni volta che incontro una nuova parentesi aperta ,creo un nuovo nodo con il campo key vuoto : 
se il nodo attuale non ha figlio sx ,assegno il nodo appena creato come figlio sx ; se non ha figlio dx come figlio dx. Altrimenti il nuovo
nodo avrà come figlio sx il nodo attuale ed il nuovo nodo sarà la radice
3) se incontro un numero ,creo un nuovo nodo con tale valore; Se il nodo attuale(che rappresenta l'operatore) è pieno (cioè
contiene un operatore) e NON ha figlio sx allora assegno tale nodo nuovo nodo al figlio dx del nodo attuale ed estraggo dalla pila il nodo attuale
che diventerà il nodo che sarà nella top della pila. Se non esiste il nodo attuale sarà la radice. Se il nodo attuale ha il campo vuoto allora
si procede come per il punto 2 
4) se incontro un operatore questo viene messo nel campo data del nodo attuale. Se tale nodo attuale non esiste ne creo un nuovo che avrà
come figlio sx la radice ; 
5) ogni volta che faccio della pop il nodo attuale diventa quello che attualmente è nella pila.
*/
template<typename T>
class Node
{
	public:
		Node( const T & input=' '):data(input),prevPtr(0),nextPtr(0){}
		Node<T>* getNextPtr()const {return nextPtr;}
		void setNextPtr(Node<T> *const p ){ nextPtr = p ;}
		Node<T> *getPrevPtr()const {return prevPtr;}
		void setPrevPtr( Node<T> *const p  ){prevPtr = p ;}
		T getData()const {return data;}
		void setData( const T & input){ data = input;}
	private:
		Node<T>* prevPtr,*nextPtr;
		T data;
};
template<typename T>
class Stack
{
	public:
		Stack():head(0){}
		void push( const T & i )
		{ 
			if(isEmpty())
				head = new Node<T>( i );
			else 
				{
					Node<T>* newNode = new Node<T>(i);
					newNode->setNextPtr(head);
					head->setPrevPtr(newNode);
					head = newNode;
				}
		}
		T pop()
		{
			
			Node<T>*toDelete = head;
			T i = head->getData();
			head = head->getNextPtr();
			if(head)head->setPrevPtr(0);
			toDelete->setNextPtr(0);delete toDelete;toDelete = 0;
			return i ;
		}
		void print()
		{
			if(isEmpty())
			{
				cout<<"\nisEmpty!\n";
			}else
				{	
					Node<T>*index = head;
					while(index!=NULL)
					{
						cout<<index->getData()<<endl;
						index = index->getNextPtr();
					}
				
				}
		}
		T getTop()const
		{
			if(isEmpty())return 0;
			return head->getData();
		}
		bool isEmpty()const {return head==0;}
	private:
		Node<T>*head;
};

template<typename T>
class NodeT
{
	public:
		NodeT(const T& input ):data(input),leftPtr(0),rightPtr(0),fatherPtr(0){}
		NodeT<T>*getLeftPtr()const {return leftPtr;}
		void setLeftPtr(NodeT<T>*const p ){leftPtr = p ;}
		NodeT<T>*getRightPtr()const {return rightPtr;}
		void setRightPtr( NodeT<T>*const p ){rightPtr= p ;}
		NodeT<T>*getFatherPtr()const {return fatherPtr;}
		void setFatherPtr( NodeT<T>*const p ){fatherPtr= p ;}
		T getData()const {return data;}
		void setData( const T & input){ data = input;}
	private:
		NodeT<T>*leftPtr,*rightPtr,*fatherPtr;
		T data;
};
template<typename T >
class ExpressionTree
{
	public:
		ExpressionTree():root(0){  stack = new Stack< NodeT<T>* >(); }
		void makeTree( string s )
		{
			NodeT<T>*act = 0 ;
			int x =0;
			while( x<s.length() )
			{
				char data =s[ x ];
				if(x==0 && data=='(' )
				{	act = root = new NodeT<T>(' ');
					stack->push( root) ;
				}
				else if( data=='(' )
				{			
					NodeT<T>*newNode = new NodeT<T>(' ' );
					if(act)
					{	
						if(act->getLeftPtr()==0)
						{
								act->setLeftPtr(newNode);
								newNode->setFatherPtr(act);
						}
						else if ( act->getRightPtr()==0)
						{	
							act->setRightPtr(newNode);
							newNode->setFatherPtr(act);
						}
						else //se il nodo attuale ha sia il figlio sx che destro .Si crea un nuovo nodo attuale
							{
								newNode->setLeftPtr(act);
								act->setFatherPtr(newNode);
								if(act==root) root = newNode;
								act = newNode;
 							}
					}
						act= newNode;
						stack->push( newNode);
					}else if ( isLetter(data)==true)
						{
							NodeT<T>*newNode = new NodeT<T>(data );
							if(act->getData()!=' ')//se il nodo che contiene l'operando è già pieno,bisogna inserire direttamente il figlio dx
							{	
								act->setRightPtr(newNode);
								newNode->setFatherPtr(act);
								stack->pop();
								act = stack->getTop();
							}else if(act->getLeftPtr()==0)
							{
								act->setLeftPtr(newNode);
								newNode->setFatherPtr(act);
							}
						}else if(isOperator( data)==true)
						{
							if(act!=NULL && ( act->getLeftPtr()==0 || act->getRightPtr()==0 ) )
							 	act->setData( data );
							 else 
							 	{
							 		NodeT<T>*newNode = new NodeT<T>(data );
							 		newNode->setLeftPtr(root);
							 		root->setFatherPtr(newNode);
							 		act = root = newNode;
							 		stack->push(newNode);
							 	}
						}
					x++;

				}	
		}
		bool isOperator(char i )
		{
			if( i=='*' || i=='/'|| i =='+' || i=='-' )return true;
			return false;
		}
		bool isLetter(char i )
		{
			for(char start ='a' ; start!='z' +1 ; start= start+1  )
				if( start == i ) return true;
				
		return false;
		}
		bool isNum( char n )
		{
			 for(int i=0;i<=9 ;i++)
			 	if( n -'0' ==i )return true;
			 return false;
		}
		void postOrder(){ postOrderHelp(root);}
		void postOrderHelp( NodeT<T>*p)
		{
			if(p)
			{
				postOrderHelp(p->getLeftPtr());
				postOrderHelp(p->getRightPtr());
				cout<<p->getData()<<"  ";
			
			}
		}
	private:
		Stack< NodeT<T>* >*stack;
		NodeT<T>*root;

};
int main()
{
	ExpressionTree<char> * et = new ExpressionTree<char>();
	et->makeTree( "((-a)+(x+y))/((+b)*(c*a))");
	et->postOrder();
}