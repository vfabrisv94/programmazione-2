/*ex2 . Lista di code statiche ,dove la coda contenuta in ogni nodo è rappresentata da un puntatore. Problema: ogni coda creata viene 
inserita in testa . ogni rimove del container rimuove l'elemento(se presente) nella coda in testa. Se questa ha zero elementi si elimina-

.  */
#include<iostream>
#include<ctime>
#include<cstdlib>
using namespace std;
template<typename T>
class Queue
{
	public:
		Queue(int dim = 3 ){
		size = dim<0? 3 : dim ; 
		ptr = new T[ size ];
		head = numEle = 0;
		tail = -1;
		}
	bool isEmpty() const { return numEle == 0;}
	bool isFull() const {return numEle == size ;}
	void enqueue( T x );
	T*  dequeue();
	void print() const ;
	private:
		T*ptr;
		int head;
		int tail;
		int size;
		int numEle;
};
template<typename T >
void Queue<T>::enqueue( T x )
{
	if( isFull()==false)
	{
		tail = ( tail+1)%size ; 
		ptr[tail] = x ;
		numEle++;	
	}else cout<<" The Queue is Full!"<<endl;
}
template<typename T>
void Queue<T>::print() const 
{
	if(isEmpty())return;
	int index = head%size;
	cout<<" [ ";
	while(index!=tail)
	{	cout<<ptr[index]<<" " ; index++;
	}
	cout<<ptr[index]<<" ]\n" ;
}
template<typename T >
T* Queue<T>::dequeue()
{
	if( isEmpty()==false)
	{
		head = ( head)%size ; 
		T* val = new T( ptr[head] );
		head++;
		numEle--;	
	return val;
	}else cout<<" The Queue is Empty!"<<endl;return 0;
}
template<typename T>
class Node
{
	public:
		Node( T*input = NULL ):nextPtr(0),prevPtr(0) { data = input==NULL? 0 : data = new T( *input) ;}
		Node<T>*getNextPtr()const {return nextPtr;}
		void setNextPtr(Node<T>*const p ) { nextPtr = p ;}
		Node<T>*getPrevPtr()const {return prevPtr;}
		void setPrevPtr(Node<T>*const p ) { prevPtr = p ;}
		T* getData() const {return data;}
		void setData(  T*const input) {data = input;}
	private:
		T*data;
		Node<T>*prevPtr,*nextPtr;
};
template<typename T>
class List
{
	public:
		List():head(0){}
		List( const List<T>& );
		List<T>* insert( T x ); 
		T*  removeFromFront();
		void print() const {
			if(isEmpty())return;
			Node<T>*index = head;
			while(index!=NULL)
			{	cout<<*(index->getData() )<<" ";index = index->getNextPtr();
			}
		}	
		const List& operator=( const List& );
		Node<T>* Copy( Node<T>* p ,T* data);
		bool isEmpty() const {return head==0;}
		Node<T>* getHead() const { return head;}
		void setHead( Node<T>* p ) {head = p;}
	private:
		Node<T>*head;
};
template<typename T>
Node<T>* List<T>::Copy( Node<T>* p ,T* data)
{
	
	if( p ==0 ) return 0 ;
	
	Node<T>*temp=  new Node<T>(p->getData());
	temp->setNextPtr( Copy( p->getNextPtr(),data) );
	temp->setPrevPtr(p);
		return temp;
	
	
	
}
template<typename T>
List<T>::List(const List<T> &listToCopy)
{
	/*cout<<" copyy";
	Node<T>*nodeL = this->getHead();
	Node<T>*nodeR = listToCopy.getHead();
	this->setHead( nodeR );
	
	Node<T>*temp = nodeL ;
	while(temp!=NULL)
	{
		nodeL = nodeR;
		nodeL = nodeL->getNextPtr();
		nodeR=nodeR->getNextPtr();
		nodeL = nodeR;
		temp = temp->getNextPtr();
	
	}*/
	head = Copy( listToCopy.getHead(),listToCopy.getHead()->getData() );

}
template<typename T>
const List<T>& List<T>::operator=( const List<T>& right)
{
	
	

	if (this!=&right)
	{
	
		Node<T>*index = head;
		if(!index) return *this;
		
		Node<T>*indexR = right.getHead();
		this->getHead()->setData( indexR->getData() );
		
		Node<T>*temp = head;
		while(temp!=NULL)
		{
			indexR = indexR->getNextPtr();
			index = index->getNextPtr();
			if(index!=NULL && indexR!=NULL)
			{
			index->setData( indexR->getData() );
			}
			
			temp =  temp->getNextPtr();
		
		}
	
	}else 
		cout<<"Non si può assegnare una lista a se stessa"<<endl;

	return *this;

}
template<typename T>
List<T>* List<T>::insert( T x)
{
	if(isEmpty())
		head = new Node<T>( &x);
	else
		{	
			Node<T>*newNode = new Node<T>(&x);
			newNode->setNextPtr( head);
			head ->setPrevPtr( newNode);
			head = newNode;
		}
return this;
}
template<typename T>
T* List<T>::removeFromFront()
{
	if(isEmpty()) return 0;
	Node<T>*newNode =  head;
	T* i = head ->getData();
	head = newNode->getNextPtr();
	newNode->setNextPtr(0);
	if(head)
		head->setPrevPtr(0);
	delete newNode; newNode =0;
	return i;
}
template<typename T>
class ListQ
{
	public:
		ListQ(){ list = new List< Queue<T> >(); srand(time(NULL) ) ; }
		void enqueue( T x );
		void insertQueue( T x );
		void print();
		T* dequeue();
	private:
		List< Queue<T> >* list ;
		bool isEmpty() const { return list->isEmpty() ; }
};
template<typename T>
void ListQ<T>::print() 
{
	if(isEmpty())return;
	Node< Queue<T> >*index = list->getHead();
	while(index!=NULL)
	{
		index->getData()->print();
		index = index->getNextPtr();
	}
	cout<<endl;
}
template<typename T>
void ListQ<T>::enqueue( T x ) 
{
	if (isEmpty()) { ListQ<T>::insertQueue( x);return ;}
 
	Node< Queue<T> >* index = list->getHead() ;
	if(index->getData()->isFull() ) 
		ListQ<T>::insertQueue( x);
	else
		index->getData()->enqueue( x );
}
template<typename T>
void ListQ<T>::insertQueue( T x ) 
{
	
	Queue<T>* newQ = new Queue<T>();
	newQ->enqueue( x);
	list->insert( *newQ );
}
template<typename T>
T* ListQ<T>::dequeue() 
{
	if (isEmpty()) return 0 ;
Node< Queue<T> >* index = list->getHead() ;
 T* i = index->getData()->dequeue();
if ( index->getData()->isEmpty() ) 
	list->removeFromFront();
	return i;
}
int main()
{	cout<<"\nex2\n";
	
	 
	 List<int>* b = new List<int>();
	 
	  b->insert(50);
	 b->insert(77);
	 	 b->insert(79);

	 List<int> a(*b);

	// a = b ; // non si attiva costruttore di copia perchè non è un'inizializzazione ma assegnamento. In tal caso si attiva la ridefinizione di =
 	// a.removeFromFront();
		 cout<<"\n a è :\n";
	 a.print();
	 cout<<"\n b è : \n";
	 b->print();
	 a.removeFromFront();
	  cout<<" a post rimozione in testa è :\n";
	  	 a.print();

	  	 cout<<"\n b è :"<<endl;

	   			 b->print();
/*
		 
	 cout<<"\n a è :"<<endl;
	 a.print();
	/*ListQ<int>* lq = new ListQ<int>();
	for(int i=0 ; i<10 ; i++)
	{	
		int n = rand()%50;
		lq->enqueue(n);
	}
	lq->print();
	cout<<"\n rimuovi \n";
	for(int i = 0; i< 5 ; i++)
	{	int *x = 	lq->dequeue();
		cout<<*x<<" è stato rimosso!\n";
	}
	lq->print();
	
		//int *x = 	lq->dequeue();
		//cout<<*x<<" è stato rimosso!\n";

	//lq->print();
	/*Queue<int>*queue = new Queue<int>( 5);
for(int i=0; i< 6 ;i++)
	queue->enqueue( i +1 );
	queue->print();
for(int i=0; i< 5 ;i++)
 {	int* x =	queue->dequeue();	cout<<*x<<" è stato rimosso\n"; }*/
	return 0;
}