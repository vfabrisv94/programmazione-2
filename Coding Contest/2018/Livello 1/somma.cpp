#include <iostream>
#include <fstream>
using namespace std;


void soluzione(int* vett1, int* vett2, int n, ofstream& out){
    for(int i = 0; i < n; i++)
        out << vett1[i] + vett2[i] << " ";
    out << endl;
}

int main(){
    ifstream in("input.txt");
    ofstream out("output.txt");


    for(int i = 0; i < 100; i++){
        int n,m;
        in >> n; in >> m;

        int* vett1 = new int[n*m];
        int* vett2 = new int[n*m];
        for(int j = 0; j < n*m; j++)
            in >> vett1[j];
        for(int j = 0; j < n*m; j++)
            in >> vett2[j];
        soluzione(vett1, vett2, n*m, out);

        delete [] vett1; delete [] vett2;
    }

    return 0;
}