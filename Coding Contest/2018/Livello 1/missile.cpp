#include <iostream>
#include <fstream>
#include <cmath>
using namespace std;

int distanza(int x1,int y1,int x2,int y2){
    return sqrt(pow(x1-x2, 2) + pow(y2-y1, 2));
}
int coordinate(int* vett, int n, int x, int y){
    int min = distanza(x,y,vett[0],vett[1]);
    int pos = 0;
    for(int i = 2; i < 2*n; i = i+2){
        if(distanza(x,y,vett[i],vett[i+1]) < min){
            min = distanza(x,y,vett[i], vett[i+1]);
            pos = i;
        }
    }
    return pos;
}



int main(){

    ifstream in("input.txt");
    ofstream out("output.txt");

    for(int i = 0; i < 10; i++){
        int n;
        int x, y;
        in >> n;
        in >> x;
        in >> y;
        int* vett = new int[n*2];
        for(int j = 0; j < n*2; j++)
            in >> vett[j];
        int pos = coordinate(vett, n, x, y);
       out << vett[pos] << " " << vett[pos+1] << endl;
    }

    in.close();
    out.close();



    return 0;
}