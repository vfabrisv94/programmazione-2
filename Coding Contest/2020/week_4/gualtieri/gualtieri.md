# La littira del Gualtieri

«Dottori dottori, ah, dottori!» l'assugliò Catarella appena che lo vitti comparire sulla porta del commissariato. «Tre volti tilifonò il dottori Latte, quello che tiene la esse in fondo! Ci voli parlare di pirsona pirsonalmente! Dice che cosa urgentissima d'urgenza è!». Il dottor Lattes, Capo di Gabinetto del Questore, soprannominato «lattes e mieles» per il suo modo di fare untuoso e parrinesco, lo avrebbe avvisato dell'assassinio di Mimmo Gualtieri.
Il dottor Gualtieri era omo accanosciuto in tutto il paese. Era padre di 8 figli, tutti cresciuti con una buona educazione e maritati, ma era soprattutto un uomo buono, macari se con un cattivo carattere. Era in pinsione da dieci anni e capitava spesso che si fermasse a chiacchierare con il commissario quando i due si incontravano lungo la spiaggia di Marinella. Mai gli aveva parlato di persone che gli volevano male.
Negli ultimi anni pirò alla moglie del Gualtieri era capitata una brutta malattia. La sua pinsione era appena bastevole per mantenere la casa e la famiglia e, per coprire le spese delle cure della moglie, Mimmo Gualtieri si era rivolto al giro dell'usura. « Evidentemente non era arrinisciuto più a pagare e questo non era andato giù ai suoi strozzini » pinsò il commissario.
La moglie del poviromo però gli contò tutto. Disse al commissario che suo marito riceveva ogni mesata una littira da uno sconosciuto. All'interno della busta, oltre alla littira, c'era macari un pizziddo di carta dove c'era scritta una sola parola. La consegna del denaro avveniva sempre in contrada Coffa, una zona residenziale piena di ville. Per trovare il numero civico al quale sarebbe avvenuta la consegna Mimmo Gualtieri doveva tenere conto di quante volte la parola scritta del pizziddo si ripeteva all'interno della littira, macari quando le lettere della parola erano state scanciate di posto.
Il commissario pinsò che molte delle ville di contrada Coffa erano dotate di telecamere di sorveglianza. Macari, se era fortunato, una di queste telecamere aveva ripreso il momento dell'incontro del Gualtieri con i suoi aguzzini. Non rimaneva quindi al Commissario che capire dove fosse avvenuto l'ultimo incontro di Mimmo Gualtieri con gli usurai.

## Specifiche
Si aiuti il Commissario Montalbano a contare il numero di volte in cui la parola scritta sul pizzino di carta occorre all'interno della lettera, anche quando i caratteri della stessa parola sono cambiati di posizione. Ciò consiste quindi nel trovare tutte le volte in cui una qualsiasi permutazione dei caratteri della parola occorre nella lettera.

## Dati in input
L'input è costituito da 100 righe, una per ogni task. Ogni riga del file di input contiene due stringhe P ed L: la prima rappresenta la parola scritta nel pizzino, la seconda rappresenta la lettera contenuta nella busta.

## Dati in output
Il file di output è composto da 100 righe, una per ogni task presente nel file di input. Ogni riga conterrà il numero di occorrenze in cui una qualsiasi permutazione della parola P è presente nella lettera L.

## Note
La lunghezza della stringa L è sempre compresa tra 10 e 650 caratteri.
La lunghezza della stringa P è sempre minore della lunghezza della stringa L.
Le due stringhe sono composte solo dai caratteri dell'alfabeto a-z ed A-Z.

### Esempio
Il seguente esempio presenta un file di input, contenente 2 task, ed il corrispondente file di output.

input.txt
aab abbabaabaa
cAda ABrAcadAbRa

output.txt
5
2

Spiegazione:
Nel secondo caso d'esempio è possibile trovare 2 permutazioni della stringa "cAda":
- ABr[Acad]AbRa
- ABrA[cadA]bRa 
