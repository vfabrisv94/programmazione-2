# Gli scheletri nella grotta

Prima d'andare in ufficio il commissario Salvo Montalbano passò dall'edicola e accattò i due giornali che si pubblicavano nell'isola. Tutti e due davano ampio rilievo alla scoperta, avvenuta il giorno avanti, dei corpi dei due giovani amanti all'interno della grotta. Il giornale che si stampava a Palermo era certo che si trattava di un suicidio per amore, quello che si stampava a Catania era aperto magari alla tesi dell'omicidio.
Il Dottor Pasquano, medico della Scientifica, poteva dire picca: - devo ancora taliàre altre cose - disse al commissario. Era però certo che i due erano morti sparati. Lui con un colpo alla tempia, lei con un colpo al cuore. A giudicare dallo stato di decomposizione dei due scheletri il suicidio, o l'ammazzatina, doveva essere capitata circa sessant'anni prima, alla fine della seconda grande guerra. La finissima rena che si era depositata sui corpi dei due giovani, trovati abbracciati l'uno all'altra, aveva in qualche modo fermato il disfacimento dei cadaveri.
Il Dottor Pasquano poteva, prima di tutto, stabilire se i due giovani erano stati uccisi all'interno della grotta o se erano stati trasportati li dopo l'ammazzatina. Per fare questo avrebbe utilizzato una macchina per l'analisi delle radiazioni al carbonio 14, in dotazione alla scientifica di Montelusa. I due scheletri ritrovati nella grotta avrebbero quindi dovuto subire un'attenta esposizione alle radiazioni per determinarne l'esatta provenienza.

## Specifiche
Si aiuti il Dottor Pasquano a capire quale sia il minor numero di volte che la macchina deve essere attivata per dare la giusta quantità di radiazioni in ogni punto della superficie delle ossa degli scheletri rinvenuti all'interno della grotta.
Ogni volta che la macchina viene attivata, il Dottor Pasquano indica il punto di partenza, X, ed il punto di terminazione, Y, sulla superficie delle ossa (si supponga X<=Y). La macchina quindi opera su tutti i centimetri dello scheletro compresi tra la posizione X e la posizione Y (estremi inclusi), che accumulano 1 unità di radiazioni.
Poiché ciascun centimetro delle ossa potrebbe dover ricevere una diversa quantità di radiazioni, potrebbe essere necessario attivare più volte la macchina per far accumulare in ogni porzione della superficie le radiazioni necessarie.

## Dati in input
L'input è composto da N+1 valori. Il primo valore N indica la lunghezza dello scheletro in centimetri, i seguenti N valori indicano la quantità di radiazioni che ciascun centimetro delle ossa deve ricevere.

## Dati in output
Il file di output è composto da 100 righe, una per ogni task presente nel file di input. Ogni riga conterrà il valore che indica il minor numero di volte che la macchina deve essere attivata per far accumulare in ogni centimetro delle ossa la giusta quantità di radiazioni.

## Note
Il valore di N sarà compreso tra 1 e 1000.
La quantità di radiazioni che ciascun centimetro della scheletro deve ricevere è compresa tra 0 e 100.
Ogni volta che la macchina viene attivata, essa opera su tutti i centimetri dal X-esimo al Y-esimo (con X

### Esempio
Il seguente esempio presenta un file di input, contenente 2 task, ed il corrispondente file di output.

input.txt:
4 1 2 3 1
4 100 0 1 1

output.txt
3
101

Spiegazione:
Nel primo esempio è sufficiente attivare tre volte la macchina. La prima attivazione permette di accumulare un'unità di radiazione dal centimetro 1 al centimetro 4. Successivamente si aggiunge un'unità di radiazione ai centimetri compresi tra 2 e 3. Infine si aggiunge una singola unità di radiazione al centimetro 3.
Il secondo esempio impone invece di attivare la macchina 100 volte per permettere l'accumulazione di 100 unità di radiazioni alla superficie presente nel primo centimetro. Infine l'ultima attivazione permette alla superficie che va dal centimetro 3 al centimetro 4 di accumulare una singola unità di radiazione.
